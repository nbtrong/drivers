﻿using DRIVERS.Services.IService;
using DRIVERS.Tools;
using DRIVERSEntity.Repository;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DRIVERS.Services.Service
{
    public class DriverHistoryService : IDriverHistoryService
    {
        public bool Create(DriverHistoryDto history)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_HISTORY.Create(history);
            }
        }
        public bool Update(DriverHistoryDto history)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_HISTORY.Update(history);
            }
        }
        public bool Delete(DriverHistoryDto history)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_HISTORY.Delete(history);
            }
        }
        public DriverHistoryDto GetById(int Id)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_HISTORY.GetById(Id);
            }
        }
        public List<DriverHistoryDto> GetByDriverId(int Id)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_HISTORY.GetByDriverId(Id);
            }
        }
    }
}