﻿using DRIVERS.Services.IService;
using DRIVERS.Tools;
using DRIVERSEntity.Repository;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DRIVERS.Services.Service
{
    public class DriverService : IDriverService
    {
        public void Connection()
        {
            using (DriversRepository rep = Repository.Instance())
            {
                rep.DRIVER.Connection();
            }
        }
        public bool Create(DriverDto d)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER.Create(d);
            }
        }
        public bool Update(DriverDto d)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER.Update(d);
            }
        }
        public bool Delete(DriverDto d)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER.Delete(d);
            }
        }
        public List<DriverDto> GetAll()
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER.GetAll();
            }
        }
        public DriverDto GetById(int Id)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER.GetById(Id);
            }
        }
        public DriverDto GetByCode(string Code)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER.GetByCode(Code);
            }
        }
    }
}