﻿using DRIVERS.Services.IService;
using DRIVERS.Tools;
using DRIVERSEntity.Repository;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DRIVERS.Services.Service
{
    public class DriverLocationService : IDriverLocationService
    {
        public bool Create(DriverLocationDto location)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_LOCATION.Create(location);
            }
        }
        public bool Update(DriverLocationDto location)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_LOCATION.Update(location);
            }
        }
        public bool Delete(DriverLocationDto location)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_LOCATION.Delete(location);
            }
        }
        public DriverLocationDto GetById(int Id)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_LOCATION.GetById(Id);
            }
        }
        public DriverLocationDto GetByDriverId(int Id)
        {
            using (DriversRepository rep = Repository.Instance())
            {
                return rep.DRIVER_LOCATION.GetByDriverId(Id);
            }
        }
    }
}