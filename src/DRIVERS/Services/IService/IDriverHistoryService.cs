﻿using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERS.Services.IService
{
    public interface IDriverHistoryService
    {
        bool Create(DriverHistoryDto history);
        bool Update(DriverHistoryDto history);
        bool Delete(DriverHistoryDto history);
        DriverHistoryDto GetById(int Id);
        List<DriverHistoryDto> GetByDriverId(int Id);
    }
}
