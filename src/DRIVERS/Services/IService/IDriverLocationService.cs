﻿using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERS.Services.IService
{
    public interface IDriverLocationService
    {
        bool Create(DriverLocationDto location);
        bool Update(DriverLocationDto location);
        bool Delete(DriverLocationDto location);
        DriverLocationDto GetById(int Id);
        DriverLocationDto GetByDriverId(int Id);
    }
}
