﻿using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERS.Services.IService
{
    public interface IDriverService
    {
        void Connection();
        bool Create(DriverDto d);
        bool Update(DriverDto d);
        bool Delete(DriverDto d);
        List<DriverDto> GetAll();
        DriverDto GetById(int Id);
        DriverDto GetByCode(string Code);
    }
}
