﻿using DRIVERS.Services.IService;
using DRIVERS.Services.Service;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DRIVERS.Controllers
{
    public class DriverHistoryController : Controller
    {
        private IDriverHistoryService historyService = null;
        public DriverHistoryController()
        {
            historyService = new DriverHistoryService();
        }
        // GET: DriverHistory
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetById(int Id)
        {
            return Json(historyService.GetById(Id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetByDriverId(int Id)
        {
            return Json(historyService.GetByDriverId(Id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(DriverHistoryDto history)
        {
            return Json(historyService.Create(history), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(DriverHistoryDto history)
        {
            return Json(historyService.Update(history), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(DriverHistoryDto history)
        {
            return Json(historyService.Update(history), JsonRequestBehavior.AllowGet);
        }
    }
}