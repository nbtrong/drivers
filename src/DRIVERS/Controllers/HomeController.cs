﻿using DRIVERS.Services.IService;
using DRIVERS.Services.Service;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DRIVERS.Controllers
{
    public class HomeController : Controller
    {
        private IDriverService driverService = null;
        public HomeController()
        {
            driverService = new DriverService();
        }
        public ActionResult Index()
        {
            driverService.Connection();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}