﻿using DRIVERS.Services.IService;
using DRIVERS.Services.Service;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DRIVERS.Controllers
{
    public class DriverController : Controller
    {
        private IDriverService driverService = null;
        public DriverController()
        {
            driverService = new DriverService();
        }
        // GET: Driver
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAll()
        {
            return Json(driverService.GetAll(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetById(int Id)
        {
            return Json(driverService.GetById(Id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetByCode(string Code)
        {
            return Json(driverService.GetByCode(Code), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(DriverDto d)
        {
            return Json(driverService.Create(d), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(DriverDto d)
        {
            return Json(driverService.Update(d), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(DriverDto d)
        {
            return Json(driverService.Update(d), JsonRequestBehavior.AllowGet);
        }
    }
}