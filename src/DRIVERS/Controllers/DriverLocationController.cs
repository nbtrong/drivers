﻿using DRIVERS.Services.IService;
using DRIVERS.Services.Service;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DRIVERS.Controllers
{
    public class DriverLocationController : Controller
    {
        private IDriverLocationService locationService = null;
        public DriverLocationController()
        {
            locationService = new DriverLocationService();
        }
        // GET: DriverLocation
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetById(int Id)
        {
            return Json(locationService.GetById(Id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetByDriverId(int Id)
        {
            return Json(locationService.GetByDriverId(Id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(DriverLocationDto location)
        {
            return Json(locationService.Create(location), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(DriverLocationDto location)
        {
            return Json(locationService.Update(location), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(DriverLocationDto location)
        {
            return Json(locationService.Update(location), JsonRequestBehavior.AllowGet);
        }
    }
}