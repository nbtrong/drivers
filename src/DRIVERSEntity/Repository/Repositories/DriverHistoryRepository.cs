﻿using DRIVERSEntity.Entity.Models;
using DRIVERSEntity.Repository.Definitions;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERSEntity.Repository.Repositories
{
    public class DriverHistoryRepository : AbstractRepository<DRIVER_HISTORY>
    {
        public DriverHistoryRepository(DriversRepository repository) : base(repository)
        {
            OnAdd += DriverHistoryRepository_OnAdd;
            OnUpdate += DriverHistoryRepository_OnUpdate;
        }
        private void DriverHistoryRepository_OnUpdate(object sender, EntityEventArgs e)
        {
            e.Entity.LAST_UPDATE = DateTime.Now;
            if (!e.Entity.CREATED_DATE.HasValue)
            {
                e.Entity.CREATED_DATE = DateTime.Now;
            }
        }

        private void DriverHistoryRepository_OnAdd(object sender, EntityEventArgs e)
        {
            e.Entity.CREATED_DATE = DateTime.Now;
            e.Entity.LAST_UPDATE = DateTime.Now;
        }

        public bool Create(DriverHistoryDto history)
        {
            try
            {
                DRIVER_HISTORY h = new DRIVER_HISTORY();
                h.ID = history.Id;
                h.DRIVER_ID = history.DriverId;
                h.TRIP_ID = history.TripId;
                h.STATUS = history.Status;

                Add(h);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Update(DriverHistoryDto history)
        {
            try
            {
                DRIVER_HISTORY h = this.GetByID(history.Id);
                h.ID = history.Id;
                h.DRIVER_ID = history.DriverId;
                h.TRIP_ID = history.TripId;
                h.STATUS = history.Status;

                Update(h);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(DriverHistoryDto history)
        {
            try
            {
                DRIVER_HISTORY h = this.GetByID(history.Id);
                h.DELETED = true;
                Update(h);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public DriverHistoryDto GetById(int Id)
        {
            try
            {
                var result = new DriverHistoryDto();
                var driver = this.DataSet.Where(d => !d.DELETED && d.ID == Id).FirstOrDefault();
                if (driver != null)
                {
                    result.Id = driver.ID;
                    result.DriverId = driver.DRIVER_ID;
                    result.TripId = driver.TRIP_ID;
                    result.Status = driver.STATUS;
                }
                return result;
            }
            catch (Exception ex)
            {
                return new DriverHistoryDto();
            }
        }
        public List<DriverHistoryDto> GetByDriverId(int Id)
        {
            try
            {
                return this.DataSet.Where(d => !d.DELETED && d.DRIVER_ID == Id)
                    .Select(d => new DriverHistoryDto
                    {
                        Id = d.ID,
                        DriverId = d.DRIVER_ID,
                        TripId = d.TRIP_ID,
                        Status = d.STATUS,
                    }).ToList();
            }
            catch (Exception ex)
            {
                return new List<DriverHistoryDto>();
            }
        }
    }
}
