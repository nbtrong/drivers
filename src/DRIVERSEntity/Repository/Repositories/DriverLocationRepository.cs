﻿using DRIVERSEntity.Entity.Models;
using DRIVERSEntity.Repository.Definitions;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERSEntity.Repository.Repositories
{
    public class DriverLocationRepository : AbstractRepository<DRIVER_LOCATION>
    {
        public DriverLocationRepository(DriversRepository repository) : base(repository)
        {
            OnAdd += DriverLocationRepository_OnAdd;
            OnUpdate += DriverLocationRepository_OnUpdate;
        }
        private void DriverLocationRepository_OnUpdate(object sender, EntityEventArgs e)
        {
            e.Entity.LAST_UPDATE = DateTime.Now;
            if (!e.Entity.CREATED_DATE.HasValue)
            {
                e.Entity.CREATED_DATE = DateTime.Now;
            }
        }

        private void DriverLocationRepository_OnAdd(object sender, EntityEventArgs e)
        {
            e.Entity.CREATED_DATE = DateTime.Now;
            e.Entity.LAST_UPDATE = DateTime.Now;
        }

        public bool Create(DriverLocationDto location)
        {
            try
            {
                DRIVER_LOCATION l = new DRIVER_LOCATION();
                l.DRIVER_ID = location.DriverId;
                l.LATITUDE = location.Latitude;
                l.LONGITUDE = location.Longitude;
                Add(l);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Update(DriverLocationDto location)
        {
            try
            {
                DRIVER_LOCATION l = this.GetByID(location.Id);
                l.LATITUDE = location.Latitude;
                l.LONGITUDE = location.Longitude;
                Update(l);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(DriverLocationDto location)
        {
            try
            {
                DRIVER_LOCATION l = this.GetByID(location.Id);
                l.DELETED = true;
                Update(l);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public DriverLocationDto GetById(int Id)
        {
            try
            {
                var result = new DriverLocationDto();
                var location = this.DataSet.Where(d => !d.DELETED && d.ID == Id).FirstOrDefault();
                if (location != null)
                {
                    result.Id = location.ID;
                    result.DriverId = location.DRIVER_ID;
                    result.Latitude = location.LATITUDE;
                    result.Longitude = location.LONGITUDE;
                }
                return result;
            }
            catch (Exception ex)
            {
                return new DriverLocationDto();
            }
        }

        public DriverLocationDto GetByDriverId(int Id)
        {
            try
            {
                var result = new DriverLocationDto();
                var location = this.DataSet.Where(d => !d.DELETED && d.DRIVER_ID == Id).FirstOrDefault();
                if (location != null)
                {
                    result.Id = location.ID;
                    result.DriverId = location.DRIVER_ID;
                    result.Latitude = location.LATITUDE;
                    result.Longitude = location.LONGITUDE;
                }
                return result;
            }
            catch (Exception ex)
            {
                return new DriverLocationDto();
            }
        }
    }
}
