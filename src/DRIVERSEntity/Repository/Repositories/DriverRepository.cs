﻿using DRIVERSEntity.Entity.Models;
using DRIVERSEntity.Repository.Definitions;
using DRIVERSEntity.Repository.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERSEntity.Repository.Repositories
{
    public class DriverRepository : AbstractRepository<DRIVER>
    {
        public DriverRepository(DriversRepository repository) : base(repository)
        {
            OnAdd += DriverRepository_OnAdd;
            OnUpdate += DriverRepository_OnUpdate;
        }
        private void DriverRepository_OnUpdate(object sender, EntityEventArgs e)
        {
            e.Entity.LAST_UPDATE = DateTime.Now;
            if (!e.Entity.CREATED_DATE.HasValue)
            {
                e.Entity.CREATED_DATE = DateTime.Now;
            }
        }
        private void DriverRepository_OnAdd(object sender, EntityEventArgs e)
        {
            e.Entity.CREATED_DATE = DateTime.Now;
            e.Entity.LAST_UPDATE = DateTime.Now;
        }
        public void Connection()
        {
            try
            {
                var Connection = this.DataSet.FirstOrDefault().ID;
            }
            catch (Exception ex)
            {

            }
        }
        public bool Create(DriverDto driver)
        {
            try
            {
                DRIVER d = new DRIVER();
                d.CODE = driver.Code;
                d.LAST_NAME = driver.LastName;
                d.FIRST_NAME = driver.FirstName;
                d.BIRTH_DAY = driver.BirthDay;
                d.GENDER = driver.Gender;
                d.PHONE = driver.Phone;
                d.EMAIL = driver.Email;
                d.ADDRESS = driver.Address;
                d.STATUS = driver.Status;
                d.USER_NAME = driver.Username;
                d.PASSWORD = driver.Password;
                Add(d);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Update(DriverDto driver)
        {
            try
            {
                DRIVER d = this.GetByID(driver.Id);
                d.CODE = driver.Code;
                d.LAST_NAME = driver.LastName;
                d.FIRST_NAME = driver.FirstName;
                d.BIRTH_DAY = driver.BirthDay;
                d.GENDER = driver.Gender;
                d.PHONE = driver.Phone;
                d.EMAIL = driver.Email;
                d.ADDRESS = driver.Address;
                d.STATUS = driver.Status;
                d.USER_NAME = driver.Username;
                d.PASSWORD = driver.Password;
                Update(d);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(DriverDto driver)
        {
            try
            {
                DRIVER d = this.GetByID(driver.Id);
                d.DELETED = true;
                Update(d);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<DriverDto> GetAll()
        {
            try
            {
                return this.DataSet.Where(d => !d.DELETED)
                    .Select(d => new DriverDto {
                        Id = d.ID,
                        Code = d.CODE,
                        LastName = d.LAST_NAME,
                        FirstName = d.FIRST_NAME,
                        BirthDay = d.BIRTH_DAY,
                        Gender = d.GENDER,
                        Phone = d.PHONE,
                        Email = d.EMAIL,
                        Address = d.ADDRESS,
                        Status = d.STATUS,
                        Username = d.USER_NAME,
                        Password = d.PASSWORD,
                    }).ToList();
            }
            catch (Exception ex)
            {
                return new List<DriverDto>();
            }
        }
        public DriverDto GetById(int Id)
        {
            try
            {
                var result = new DriverDto();
                var driver = this.DataSet.Where(d => !d.DELETED && d.ID == Id).FirstOrDefault();
                if (driver != null)
                {
                    result.Id = driver.ID;
                    result.Code = driver.CODE;
                    result.LastName = driver.LAST_NAME;
                    result.FirstName = driver.FIRST_NAME;
                    result.BirthDay = driver.BIRTH_DAY;
                    result.Gender = driver.GENDER;
                    result.Phone = driver.PHONE;
                    result.Email = driver.EMAIL;
                    result.Address = driver.ADDRESS;
                    result.Status = driver.STATUS;
                    result.Username = driver.USER_NAME;
                    result.Password = driver.PASSWORD;
                }
                return result;
            }
            catch (Exception ex)
            {
                return new DriverDto();
            }
        }
        public DriverDto GetByCode(string Code)
        {
            try
            {
                var result = new DriverDto();
                var driver = this.DataSet.Where(d => !d.DELETED && d.CODE == Code).FirstOrDefault();
                if (driver != null)
                {
                    result.Id = driver.ID;
                    result.Code = driver.CODE;
                    result.LastName = driver.LAST_NAME;
                    result.FirstName = driver.FIRST_NAME;
                    result.BirthDay = driver.BIRTH_DAY;
                    result.Gender = driver.GENDER;
                    result.Phone = driver.PHONE;
                    result.Email = driver.EMAIL;
                    result.Address = driver.ADDRESS;
                    result.Status = driver.STATUS;
                    result.Username = driver.USER_NAME;
                    result.Password = driver.PASSWORD;
                }
                return result;
            }
            catch (Exception ex)
            {
                return new DriverDto();
            }
        }
    }
}
