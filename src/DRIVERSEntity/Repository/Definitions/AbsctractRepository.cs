﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERSEntity.Repository.Definitions
{
    public class AbstractRepository<T> : IRepository<T> where T : class
    {

        public class EntityEventArgs : EventArgs
        {
            public T Entity { get; set; }

            public EntityEventArgs(T entity)
            {
                Entity = entity;
            }
        }

        //PARENT
        protected DriversRepository Repository;
        protected DbSet<T> DataSet;

        //event trigger
        protected event EventHandler<EntityEventArgs> OnAdd;
        protected event EventHandler<EntityEventArgs> OnUpdate;
        protected event EventHandler<EntityEventArgs> OnDelete;

        public AbstractRepository(DriversRepository repository)
        {
            Repository = repository;
            DataSet = repository.Context.Set<T>();
        }
        public T GetByID(object key)
        {
            return DataSet.Find(key);
        }
        public void Add(T entity)
        {
            OnAdd?.Invoke(this, new EntityEventArgs(entity));
            DataSet.Add(entity);
        }
        public void Update(T entity)
        {
            OnUpdate?.Invoke(this, new EntityEventArgs(entity));
            T databaseEntity = (Repository.Context as IObjectContextAdapter).ObjectContext.ObjectStateManager.GetObjectStateEntry(entity).Entity as T;
            foreach (var property in typeof(T).GetProperties())
                property.SetValue(databaseEntity, property.GetValue(entity));
        }
        public void Delete(T entity)
        {
            OnDelete?.Invoke(this, new EntityEventArgs(entity));
            if (Repository.Context.Entry<T>(entity).State == EntityState.Detached)
                DataSet.Attach(entity);
            DataSet.Remove(entity);
        }
        public void Delete(object key)
        {
            Delete(GetByID(key));
        }
    }
}
