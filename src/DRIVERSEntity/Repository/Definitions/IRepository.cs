﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERSEntity.Repository.Definitions
{
    public interface IRepository<T> where T : class
    {
        T GetByID(Object key);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Object key);
    }
}
