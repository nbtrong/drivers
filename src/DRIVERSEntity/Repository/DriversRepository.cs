﻿using DRIVERSEntity.Entity.Context;
using DRIVERSEntity.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERSEntity.Repository
{
    public class DriversRepository : IDisposable
    {
        protected DRIVERSContext _context;
        public DRIVERSContext Context
        {
            get
            {
                return _context;
            }
        }

        #region Repository
        public DriversRepository()
        {
            _context = new DRIVERSContext();
        }
        ~DriversRepository()
        {
            Dispose();
        }
        public int SaveChanges()
        {
            _context.ChangeTracker.DetectChanges();
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && _context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }
        #endregion

        #region DRIVER
        private DriverRepository _DRIVER;
        public DriverRepository DRIVER
        {
            get
            {
                if (_DRIVER == null)
                    _DRIVER = new DriverRepository(this);
                return _DRIVER;
            }
        }
        #endregion

        #region DRIVER_LOCATION
        private DriverLocationRepository _DRIVER_LOCATION;
        public DriverLocationRepository DRIVER_LOCATION
        {
            get
            {
                if (_DRIVER_LOCATION == null)
                    _DRIVER_LOCATION = new DriverLocationRepository(this);
                return _DRIVER_LOCATION;
            }
        }
        #endregion

        #region DRIVER_HISTORY
        private DriverHistoryRepository _DRIVER_HISTORY;
        public DriverHistoryRepository DRIVER_HISTORY
        {
            get
            {
                if (_DRIVER_HISTORY == null)
                    _DRIVER_HISTORY = new DriverHistoryRepository(this);
                return _DRIVER_HISTORY;
            }
        }
        #endregion
        public System.Data.Entity.DbSet<T> GetRepository<T>() where T : class
        {
            return this.Context.Set<T>();
        }
    }
}
