﻿using DRIVERSEntity.Entity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices;

namespace DRIVERSEntity.Entity.Context
{
    public partial class DRIVERSContext : DbContext
    {
        public DRIVERSContext() : base("name=DRIVERSContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DRIVERSContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DRIVERSContext, Migrations.Configuration>("DRIVERSContext"));
            Database.CommandTimeout = 600;
            Configuration.AutoDetectChangesEnabled = false;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
        public virtual DbSet<DRIVER> DRIVER { get; set; }
        public virtual DbSet<DRIVER_HISTORY> DRIVER_HISTORY { get; set; }
        public virtual DbSet<DRIVER_LOCATION> DRIVER_LOCATION { get; set; }
    }
}
