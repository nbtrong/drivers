﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERSEntity.Entity.Models
{
    [Table("DRIVER")]
    public class DRIVER
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Column(TypeName = "nvarchar")]
        public string CODE { get; set; }
        [Column(TypeName = "nvarchar")]
        public string LAST_NAME { get; set; }
        [Column(TypeName = "nvarchar")]
        public string FIRST_NAME { get; set; }
        public DateTime? BIRTH_DAY { get; set; }
        public int? GENDER { get; set; }
        [Column(TypeName = "nvarchar")]
        public string PHONE { get; set; }
        [Column(TypeName = "nvarchar")]
        public string EMAIL { get; set; }
        [Column(TypeName = "nvarchar")]
        public string ADDRESS { get; set; }
        public bool STATUS { get; set; }
        [Column(TypeName = "varchar")]
        public string USER_NAME { get; set; }
        [Column(TypeName = "varchar")]
        public string PASSWORD { get; set; }
        public bool DELETED { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public DateTime LAST_UPDATE { get; set; }
    }
}
