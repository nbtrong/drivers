﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRIVERSEntity.Entity.Models
{
    [Table("DRIVER_LOCATION")]
    public class DRIVER_LOCATION
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int DRIVER_ID { get; set; }
        public double LATITUDE { get; set; }
        public double LONGITUDE { get; set; }
        public bool DELETED { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public DateTime LAST_UPDATE { get; set; }
        [ForeignKey("DRIVER_ID")]
        public virtual DRIVER DRIVER { get; set; }
    }
}
