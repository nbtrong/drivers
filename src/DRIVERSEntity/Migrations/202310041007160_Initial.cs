﻿namespace DRIVERSEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DRIVER",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CODE = c.String(maxLength: 4000),
                        LAST_NAME = c.String(maxLength: 4000),
                        FIRST_NAME = c.String(maxLength: 4000),
                        BIRTH_DAY = c.DateTime(),
                        GENDER = c.Int(),
                        PHONE = c.String(maxLength: 4000),
                        EMAIL = c.String(maxLength: 4000),
                        ADDRESS = c.String(maxLength: 4000),
                        STATUS = c.Boolean(nullable: false),
                        USER_NAME = c.String(maxLength: 8000, unicode: false),
                        PASSWORD = c.String(maxLength: 8000, unicode: false),
                        DELETED = c.Boolean(nullable: false),
                        CREATED_DATE = c.DateTime(),
                        LAST_UPDATE = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DRIVER_HISTORY",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DRIVER_ID = c.Int(nullable: false),
                        TRIP_ID = c.Int(nullable: false),
                        STATUS = c.String(maxLength: 4000),
                        DELETED = c.Boolean(nullable: false),
                        CREATED_DATE = c.DateTime(),
                        LAST_UPDATE = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DRIVER", t => t.DRIVER_ID)
                .Index(t => t.DRIVER_ID);
            
            CreateTable(
                "dbo.DRIVER_LOCATION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DRIVER_ID = c.Int(nullable: false),
                        LATITUDE = c.Double(nullable: false),
                        LONGITUDE = c.Double(nullable: false),
                        DELETED = c.Boolean(nullable: false),
                        CREATED_DATE = c.DateTime(),
                        LAST_UPDATE = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DRIVER", t => t.DRIVER_ID)
                .Index(t => t.DRIVER_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DRIVER_LOCATION", "DRIVER_ID", "dbo.DRIVER");
            DropForeignKey("dbo.DRIVER_HISTORY", "DRIVER_ID", "dbo.DRIVER");
            DropIndex("dbo.DRIVER_LOCATION", new[] { "DRIVER_ID" });
            DropIndex("dbo.DRIVER_HISTORY", new[] { "DRIVER_ID" });
            DropTable("dbo.DRIVER_LOCATION");
            DropTable("dbo.DRIVER_HISTORY");
            DropTable("dbo.DRIVER");
        }
    }
}
